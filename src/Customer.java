public class Customer {
    public static void main(String[] args) {
        CurrentAccount gopal = new CurrentAccount(50000);
        SavingAccount amrita = new SavingAccount(100000);
        gopal.deposite(10000);
        gopal.withdrawals(60455);
        System.out.println("Balance in Gopal's Account");
        System.out.println(gopal.balanceInAccount);

        amrita.withdrawals(45000);
        amrita.calculateInterest() ;
        System.out.println("Balance in Amrita's Account");
        System.out.println(amrita.balanceInAccount );


    }
}
