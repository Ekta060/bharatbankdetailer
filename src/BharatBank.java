public class BharatBank {

    double balanceInAccount;

    public BharatBank(double balance) {
        this.balanceInAccount = balance;
    }

    public void deposite(double depositeAmount ){
        this.balanceInAccount+=depositeAmount;
    }

    public void withdrawals(double withdrawalAmount){
        double canWithdraw=this.balanceInAccount +((20*this.balanceInAccount )/100);

        if(canWithdraw<withdrawalAmount ){
            System.out.println("Insufficient Balance");
        }
        else{
            this.balanceInAccount-=withdrawalAmount ;
        }
    }
}
