class SavingAccount extends BharatBank {

    double interestRatePerAnnum =3;

    public SavingAccount(double balance) {
        super(balance);

    }

    public void calculateInterest(){
        double totalInterestOfYear= (this.balanceInAccount * interestRatePerAnnum)/100;
        double quarterInterest=(totalInterestOfYear*3)/12 ;
        this.balanceInAccount +=quarterInterest  ;
    }

}

